//
//  AppDelegate.h
//  peihacks
//
//  Created by Nolan Phillips on 2016-03-05.
//  Copyright © 2016 Nolan Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

