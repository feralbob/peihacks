//
//  ViewController.m
//  peihacks
//
//  Created by Nolan Phillips on 2016-03-05.
//  Copyright © 2016 Nolan Phillips. All rights reserved.
//

#import "ViewController.h"
#import <MOBProj4.h>
#import <MOBProjection.h>
#import <MOBCoordinate.h>

#import "Proj/MOBProjectionEPSG3857.h"
#import <MOBProjectionEPSG4326.h>
#import <MOBProjectionEPSG5514.h>
@import MapKit;



@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lat;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *lng;
@property (weak, nonatomic) IBOutlet UIPickerView *routePicker;
@property NSArray *roads;

@end

@implementation ViewController

- (void)viewDidLoad {
    self.mapView.delegate = self;
    [super viewDidLoad];
    [self centerMap];
    [self loadRoadData];
    [self centerOnRoadAtIndex: 0];
   }

- (void) centerMap {
    float lat = 46.223519;
    float lon = -63.154113;
    CLLocationCoordinate2D coords =  CLLocationCoordinate2DMake(lat, lon);

    [self.mapView setCenterCoordinate: coords ];
    
    MKCoordinateRegion region;
    region.span.latitudeDelta = 0.60;
    region.span.longitudeDelta = 0.60;
    region.center = coords;
    [self.mapView setRegion:region animated:YES];
    [self.mapView regionThatFits:region];
}

- (void) loadRoadData {
    NSString *fileName = @"roadData";
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    
    NSData *roadJson = [NSData dataWithContentsOfFile:filePath];
    self.roads = [NSJSONSerialization JSONObjectWithData:roadJson options:NSJSONReadingMutableContainers error:NULL];
    NSLog(@"%@", self.roads);

}

- (void) centerOnRoadAtIndex: (long)index
{
    NSDictionary *roadProperties = [self.roads[index] objectForKey:@"properties"];
    
    NSNumber *lon = [roadProperties objectForKey:@"center_x"] ;
    NSNumber *lat = [roadProperties objectForKey:@"center_y"];
    
    
    MOBProj4 *proj4 = [[MOBProj4 alloc] init];
    
    MOBProjection *projFrom = [[MOBProjectionEPSG5514 alloc] init];
    MOBProjection *projTo = [[MOBProjectionEPSG4326 alloc] init];
    
    MOBCoordinate *coor = [[MOBCoordinate alloc] initWithArrayXY:@[lat,lon]];
    MOBCoordinate *coorTo = [proj4 transformFromProjection:projFrom toProjection:projTo coordinate:coor];
    
    NSLog(@"Coordinates %@ in EPSG:%lu", coorTo, [projTo epsg]);
    
    CLLocationCoordinate2D coords =  CLLocationCoordinate2DMake(coorTo.x, coorTo.y);
    [self.mapView setCenterCoordinate: coords ];
    
//    CLLocationCoordinate2D coords =  CLLocationCoordinate2DMake(lat.floatValue/100000, lon.floatValue/100000);
//    [self.mapView setCenterCoordinate: coords ];
    
}

// Picker View
- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.roads.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSDictionary *roadProperties = [self.roads[row] objectForKey:@"properties"];

    NSString *startName = [roadProperties objectForKey:@"summer_name_start"];
    NSString *endName =  [roadProperties objectForKey:@"summer_name_end"];
    
    return [NSString stringWithFormat:@"%@ - %@", startName, endName];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self centerOnRoadAtIndex:row];
}
// Memory
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Map View
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    self.lat.text = [NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.latitude];
    self.lng.text = [NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.longitude];
}

@end
