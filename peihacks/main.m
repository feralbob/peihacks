//
//  main.m
//  peihacks
//
//  Created by Nolan Phillips on 2016-03-05.
//  Copyright © 2016 Nolan Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
